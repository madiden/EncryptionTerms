# Certificate Tutorial
This is an informative project which includes general information regarding certificates.
Firstly I want to define some general terms about Certificates

* **.csr:** This is a Certificate Signing Request. Some applications can generate these for submission to certificate-authorities. The actual format is PKCS10 which is defined in 
[RFC 2986](https://tools.ietf.org/html/rfc2986). It includes some/all of the key details of the requested certificate such as subject, organizaton, state, whatnot, as well as the _public key_ of the certificate to get signed. These get signed by the CA (Certificate Authority) and a certificate is returned. The returned certificate is the public _certificate_
(which includes the public key but not the private key) which itself can be in a couple of formats.
* **.pem:** Defined in RFC's [1421](https://tools.ietf.org/html/rfc1421) through [1424](https://tools.ietf.org/html/rfc1424), this is a container format that may include just the public certificate (such as with Apache installs, and CA certificate files ``/etc/ssl/certs``), or may include an entire certificate chain including oublic key, private key, and root certificates. Confusingly, it  may also encode a CSR (e.g. as used [here](https://jamielinux.com/docs/openssl-certificate-authority/create-the-intermediate-pair.html)) as the PKCS10 format can be translated into PEM. The name is from [Privacey Enhanced Mail (PEM)](https://en.wikipedia.org/wiki/Privacy-enhanced_Electronic_Mail), a failed method for secure email but the container format it used lives on, and is a base64 translation of the x509 ASN.1 keys.
* **.key:** This is a PEM formatted file containing just the private-key of a specific certificate and is merely a conventional name not a standardized one, In Apache installs, this frequently resides in ``/etc/ssl/private``. The rights on these files are very important, and some programs will refuse to load these sertificates if they are set wrong.
* **.pkcs12 .pfx .p12:** Originally defined by RSA in the [Public-Key Cryptography Standards](https://www.rsa.com/rsalabs/node.asp?id=2124), the "12" variant was enhanced by Microsoft. This is a passworded container format that contains both public and private certificate pairs. Unlike .pem files, this container is fully encrypted. Openssl can turn this into a .pem file with both public and private keys: 

    ``openssl pkcs12 -in file-to-convert.p12 -out converted-file.pem -nodes``
    
A few other formats that show up from time to time:

* **.der:** A way to encode ASN.1 syntax in binary, a.pem files is just a Base64 encoded .der file. OpenSSL can convert these to .pem (``openssl x509 -inform der -in to-convert.der -out converted,pem``) Windows sees these as Certificate files. By default, Windows will export certificates as .DER formatted files with a different extension. Like...
* **.cert .cer .crt:** A .pem (or rarely .der) formatted file with a different extension, one that is recognized by Windows Explorer as a certificate, which .pem is not.
* **.p7b:** Defined in [RFC 2315](https://tools.ietf.org/html/rfc2315), this is a format used by windows for certificate interchamge.Java understands these natively. Unlike .pem style certificates, this format has a _defined_ way to include certification-path certificates.
* **.crl:** A certificate revocation list. Certificate Authorities produce these as a way to de-authorize certificates before expiration, You can sometimes download them from CA Websites.
* 
In summary, there are 4 different ways to present certificates and their components:

* **PEM** Governed by RFCs, it is used preferentially by open-source software. It can have a variety of extensions (.pem, .key, .cer, .cert, more)
* **PKCS7** An open standard used by Java and supported by Windows. Does not contain private key material.
* **PKCS12** A private standard that provides enhanced security versus the plain-text PEM format. This can contain private key material. It"s used preferentiakky by Windoes systems, and can be freely converted to PEM format through use of openssl.
* **DER** Tne parent format of PEM. It is useful to think of it as a binary version of the base64-encoded PEM file. Not routinely used by much outside of Windows.

In summary:
PEM on it's own isn't a certificate, it's just a way of encoding data. X.509 certificates are one type of data that is commonly encoded using PEM.

PEM is a X.509 certificate (whose structure is defined using ASN.1), encoded using the ASN.1 DER (distinguished encoding rules), then run through Base64 encoding and stuck between plain-text anchor lines (BEGIN CERTIFICATE and END CERTIFICATE).

You can represent the same data using the PKCS#7 or PKCS#12 representations, and the openssl command line utility can be used to do this.

The obvious benefits of PEM is that it's safe to paste into the body of an email message because it has anchor lines and is 7-bit clean.

RFC1422 has more details about the PEM standard as it related to keys and certificates.